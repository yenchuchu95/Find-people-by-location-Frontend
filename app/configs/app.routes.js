// var myApp = angular.module('myApp', ['ui.router']);
//
// myApp.config(function($stateProvider, urlRouterProvider) {
//     $urlRouterProvider.otherwise('/home'); //Mọi đường dẫn không hợp lệ đều được chuyển đến state home
//
//     $stateProvider
//         .state('product', { // Định ngĩa 1 state product
//         url: '/product', // khai báo Url hiển thị
//         templateUrl: 'product/product.view.html', //đường dẫn view
//         controller: 'PrtController' //// Khai báo 1 controller cho state product
//     })
//
//         .state('home', {
//             url: '/home',
//             templateUrl: 'home/home.view.html'
//         })
//
//         .state('about', {
//             url: '/about',
//             templateUrl: 'about/about.view.html'
//         })
//
//
//     // $stateProvider
//     //     .state('home', {
//     //         url: '/home',
//     //         templateUrl: 'app/components/dashboard/index.html',
//     //         controller: 'dashboardController'
//     //     })
//     //     .state('test', {
//     //         url: '/test',
//     //         templateUrl: 'app/components/TOEIC/index.html',
//     //         controller: 'testController'
//     //     })
//     //     .state('product', { // Định ngĩa 1 state product
//     //         url: '/product', // khai báo Url hiển thị
//     //         templateUrl: 'product/product.view.html', //đường dẫn view
//     //         controller: 'PrtController' //// Khai báo 1 controller cho state product
//     //     })
//     //     .state('about', {
//     //         url: '/about',
//     //         templateUrl: 'about/about.view.html'
//     //     })
// });

var routerApp = angular.module('routerApp');

routerApp.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home'); //Mọi đường dẫn không hợp lệ đều được chuyển đến state home

    $stateProvider

        .state('product', { // Định ngĩa 1 state product
            url: '/product', // khai báo Url hiển thị
            templateUrl: 'product/product.view.html', //đường dẫn view
            controller: 'PrtController' //// Khai báo 1 controller cho state product
        })
        .state('home', {
            url: '/home',
            templateUrl: 'app/home/home.view.html'
        })

        .state('login', {
            url: '/login',
            templateUrl: 'app/components/login/views/index.html',
            controller: 'loginController'
        })

        .state('dashboard', {
            url: '/dashboard',
            templateUrl: 'app/components/dashboard/index.html',
            controller: 'dashboardController'
        })
        .state('about', {
            url: '/about',
            templateUrl: 'about/about.view.html'
        })

});