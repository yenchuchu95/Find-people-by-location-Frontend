/**
 * Created by akira on 4/13/17.
 */
(function(angular){
    var app = angular.module('routerApp', [
        // controller
        'aki.directive',
        'loc.login',
        'location.dashboard',
        'myApp',

        // libs
        'ui.router' // sử dụng để gọi routes (state)
    ]);
    app.config(function () {

    });

    app.run(function() {


    });


})(angular);
