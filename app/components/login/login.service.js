"use strict";

angular.module('loc.login')
    .factory('loginService', loginService);

// $q: để trả về 1 promise
// $http: gọi ajax
loginService.$inject = ['$q', '$http'];
function loginService($q, $http) {
    var service = {};

    service.logIn = function(data) {
        var deferred = $q.defer(); // khởi tạo 1 promise
        var urlAPI = 'http://127.0.0.1:8000/api/users';
        $http({
            method: 'GET',
            url: urlAPI,
            params: data
        }).then(function successCallback(response) {
            if (response.data.code === 1) {
                deferred.resolve(response.data.user);
            } else {
                deferred.reject(response.data);
            }
        }, function errorCallback(response) {
            deferred.reject(response);
        });

        return deferred.promise;
    };

    return service;
}