'use strict';

angular.module('loc.login')
    .controller('loginController', loginController);

loginController.$inject = ['$scope', 'loginService']; // khai bao de su dung
function loginController($scope, loginService) { // gan lai vao bien tuong ung thứ tự
    $scope.name = 'login';

    $scope.logIn = function(data) {
        loginService.logIn(data)
            .then(function(res) {
                console.log('login controller success - ');
                console.log(res);
            })
            .catch(function(err) {
                console.log('login controller error - ');
                console.log(err);
            })
    }
}